//
//  ViewController.swift
//  FireplaceSwift
//
//  Created by Mihai Erős on 12/12/2016.
//  Copyright © 2016 Mihai Eros. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        createFire()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func createFire() {
        let fireEmitter = CAEmitterLayer()
        fireEmitter.emitterPosition = CGPoint(x: self.view.frame.width/2, y: 520)
        fireEmitter.emitterSize = CGSize(width: 150, height: 10);
        fireEmitter.renderMode = kCAEmitterLayerAdditive;
        fireEmitter.emitterShape = kCAEmitterLayerLine
        fireEmitter.emitterCells = [createFireCell()];
        
        self.view.layer.addSublayer(fireEmitter)
    }
    
    func createFireCell() -> CAEmitterCell {
        let fire = CAEmitterCell();
        fire.alphaSpeed = -0.3
        fire.birthRate = 570;
        fire.lifetime = 60.0;
        fire.lifetimeRange = 0.5
        fire.color = UIColor.init(colorLiteralRed: 0.8, green: 0.4, blue: 0.2, alpha: 0.6).cgColor
        fire.contents = UIImage(named: "fire")?.cgImage
        fire.emissionLongitude = CGFloat(M_PI);
        fire.velocity = 120;
        fire.velocityRange = 5;
        fire.emissionRange = 0.8;
        fire.yAcceleration = -200;
        fire.scaleSpeed = 0.6;
        
        return fire
    }

}

